FROM telegraf

COPY ping.conf /etc/telegraf/telegraf.d/ping.conf
RUN sed -i 's/127.0.0.1/influxdb/g' /etc/telegraf/telegraf.conf
