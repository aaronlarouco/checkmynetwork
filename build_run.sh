#!/bin/bash

set -x

docker stop chronograf telegraf influxdb

docker build -t telegraf-dev .
docker run --rm -d -p 8086:8086 -p 8088:8088 --name influxdb influxdb
docker run --rm -d --link influxdb:influxdb --name telegraf telegraf-dev
docker run --rm -d -p 8888:8888 --link influxdb:influxdb --name chronograf chronograf
echo "chronograf running on 0.0.0.0:8888"
